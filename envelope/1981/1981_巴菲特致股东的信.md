### 致伯克希尔-哈撒韦公司股东： 

1981年的营业利益约为3,970万美元，期初股东权益报酬率(持有股权投资以原始成本计)从去年的17.8%滑落至15.2%。我们的新计划是让所有股东皆能指定捐赠的慈善机构，(详如后述)，这使得当年度盈余减少90万美元，往后将视我们公司所得税负状况决定捐赠的金额。

#### 无控制权持股之盈余

去年我们曾详细地讨论无控制权持股盈余的观念，亦即那些我们无法控制或影响其盈余分配的一些重要的被投资公司(我们很乐意与新股东或潜在股东讨论这项话题)，而这部份的盈余却又完全未显现在Berkshire的帐面之上。

然而，我们深信这些未分配且未被记录的盈余，仍将终与那些我们控制的公司所赚的盈余一样转化成Berkshire实质的价值。

虽然我们明瞭这些不具控制权的盈余转化为Berkshire已实现或未实现利得的时间点很难拿捏，但就长期而言，市场价格或许变动不一，却终将会与企业价值同步发展，甚至不同的公司也会有不同的情况，一家将盈余运用得当与运用不当的公司，也会有不一样的结局，总而言之，只要经过合理的挑选，一群不具控制权的投资也会产生令人意想不到的效果。

就整体而言，我们这些不具控制权的被投资公司，其企业竞争力反而比那些具控制权的公司来得佳，可能的原因是因为在股票市场上我们可以合理的价格买到部份优秀企业的股权，而若要透过购并谈判的方式买下整家公司，其平均价格可能远高于市价。

#### 企业购并行为常态

经验显示，我们对于拥有整家公司或仅持有部份股权，并无特殊偏好，目前我们仍持续投资大笔资金在两者之上(我们尽量避免小额投资，因为一件事若一点也不值得去作，那么就算把它作得再好也没有用)，而事实上，由于经营保险公司与礼券事业都必须保持适当的流动性，所以我们本来就必须将资金大量投入于有价证券。

我们购并的决策著重于让实质的经济利益而非企业版图或会计数字极大化，(就长期而言，若管理当局过度注重会计数字而忽略经济实质的话，最后通常两者都顾不好)。

不管对短期的帐面盈余有何影响，我们宁愿以X价格买下一家好公司10%的股权，也不要以2X价格买下这家好公司100%的股权，但大部份公司经营阶层的选择刚好相反，而且对这样的行为总是能找到藉口。

对于这样的行为，我们可以归纳出三大动机(通常是心照不宣)：

(1)领导阶层几乎很少人是缺乏动物天性的，且时时散发出过动与战斗的意念。相对地在Berkshire，即使是购并成功在望，其管理当局的心跳也不会加快一下。

(2)大部份的公司及其经营阶层，多以“规模”而非“获利”，作为衡量自己或他人的标准(问问那些名列Fortune500大企业的负责人，他们可能从来都不知道自己的公司若以获利能力来排的话，会落在第几位)

(3)许多经营阶层很明显地过度沉浸于小时候所听到的，一个变成蟾蜍的王子因美丽的公主深深一吻而获救的童话故事，而一厢情愿地认为只要被他们优异的管理能力一吻，被购并的公司便能脱胎换骨。

如此乐观的态度确有其必要，否则公司的股东怎么会甘愿以二倍的价钱买下这家公司，而不是用一倍的价格自己从市场上买进。

换言之，投资人永远可以以蟾蜍的价格买到蟾蜍，但若投资人愿意用双倍的代价资助公主去亲吻蟾蜍的话，最好保佑奇蹟会发生，许多公主依然坚信她们的吻有使蟾蜍变成王子的魔力，即使在她的后院早已养满了一大堆的蟾蜍。

尽管如此，平心而论仍然有两种情况的购并是会成功的：

(1)第一类是你买到的(不管是有意或无意的)是那种特别能够适应通货膨胀的公司，通常它们又具备了两种特徵，一是很容易去调涨价格(即使是当产品需求平缓而产能未充份利用也一样)且不怕会失去市场佔有率或销售量；一是只要增加额外少量的资本支出，便可以使营业额大幅增加(虽然增加的原因大部份是因为通货膨胀而非实际增加产出的缘故)，近十几年来，只要符合以上两种条件，虽然这种情况不多，即使是能力普通的经理人也能使这样的购并案圆满成功。

(2)第二类是那些经营奇才，他们具有洞悉少数裹著蟾蜍外衣的王子，并且有能力让它们脱去伪装，我们尤其要向Capital City的Tom Murphy致敬，他是那种能将购并目标锁定在第一类的公司，而本身具有的管理长才又使他成为第二类的佼佼者。直接或间接的经验使我们深深体认，要达到像他们那样成就的困难性(当然也因为如此，近几年来真正成功的个案并不多，且会发现到头来利用公司资金买回自家股份是最实在的方法)。

而很不幸的，你们的董事长并不属于第二类的人，且尽管已充份体认到须将重点摆在第一类的公司，但真正命中的机率却又少之又少，我们讲得比作得好听(我们忘了诺亚的叮咛：能预测什么时候下大雨没有用，必须要能建造方舟才算。)

我们曾以划算的价钱买下不少蟾蜍，过去的报告多已提及，很明显的我们的吻表现平平，我们也遇到几个王子级的公司，但是早在我们买下时他们就已是王子了，当然至少我们的吻没让他们变回蟾蜍，而最后我们偶尔也曾成功地以蟾蜍般的价格买到部份王子级公司的股票。

#### Berkshire的购并目标

我们将持续以合理的价钱买下整家公司，即使那家公司未来的发展与过去一般；我们也愿意以较高的价钱买下第一类的公司，前提是我们可以合理的相信他们就是那样的公司；但我们通常不会去买那些我们必须替其作许多改变的公司，因为经验显示我们所作的改变不见得是好的。

今年(1981年)我们几乎谈成一笔大买卖，那家公司与其经营阶层都是我们所喜爱的，但就是价钱谈不陇，若坚持买下的结果，股东的利益不见得会比买之前更好，整个Berkshire帝国版图可能会变的更大，但人民素质反而会变差。尽管1981年我们并没有成功地谈成任何一个个案，但我们预计未来仍能买到100%符合我们标准的公司，此外我们也期望能有像后面报告所述Pinkerton这样投资大量不具投票权股权的例子，在身为次要大股东的我们可获得可观的经济利益的同时，亦能帮助公司原有的经营阶层实现其长期的目标。

我们也发现很容易从市场买到一些由有能力且正直的人所经营的公司股票，而事实上我们也从未打算自己去经营这些公司，但我们的确想要藉由投资这些公司而获利。

而我们也预期这些公司的未分配盈余(在扣除所得税后)将会100%回餽给Berkshire及其股东，当然若最后没有，可能是出了以下几种差错(1)我们所选择的经营阶层有问题(2)公司的前景有问题(3)我们付的价格有问题。

而事实上，我们不论在买进具控制权或不具控制权的股权时，皆曾犯了许多错误，其中以第二类误判的情况最常见，当然要翻开我们投资的歷史才能找得到这样的案例(可能至少要回溯至少二、三个月以上吧…)，例如去年本人就曾发表看好铝业发展的前景，只是到后来陆续经过些微的调整，最后的结论却是一百八十度的转弯。

然而基于个人与客观的理由，通常我们在改正对不具控制权股票投资的错误要比对具控制权的来得容易许多，这时候缺少控制权，反而成为一种优势。

而就像去年我曾提到的，我们在不具控制权的股权投资依投资比例可分得之未分配盈余其规模甚至超越我们公司本身的帐面盈余，且我们预期这种情况将会持续下去，1982年光是其中四家(GEICO、General Foods、R. J. Reynolds及华盛顿邮报) 加起来就超过3, 500万美元，由于会计原则规定，使得我们在计算帐面股东权益报酬与单一年度获利表现时，无法将这些未分配盈余记入。

#### 企业长期绩效表现

在衡量一家公司长期的绩效表现时，我们保险子公司所持有的股票会以市价(扣除预估应付所得税)，而若我们前面所作的推论正确的话，那些不具控制权的股权其未分配盈余，虽然不规则但最后终究会反映在我们公司帐上，至少到目前为止情况确是如此。

当然严格来说，还必须把债券投资及非保险子公司所持有的股票以市价计算才更准确，然而GAAP(一般公认会计原则)并未如此规定，而且这样做对我们来说其实影响也不大，当然若其影响大到一定程度，(就像目前很多保险同业便是如此)，我一定会向各位报告。

在GAAP的基础下，公司的帐面价值，自现有经营阶层接手的17年以来，已从19.46美元增加到如今的526.02美元，年复合成长率约为21.1%，只是这个比率在未来将会逐年下滑，但我们期望它至少能够高于一般美国大企业的平均水准。

在1981年净值增加的1亿2,000万美元中，约有一半要归功于GEICO一家公司，总的来说，今年我们投资股票市值的增加要比其背后实际代表的实质价值增加数要大得多，而请注意股票市值的表现不会永远比实质价值好。

过去我们曾解释通货膨胀是如何使我们的帐面表现比经济实质要好看的多，我们对Fed(联邦准备理事会)主席Volker先生所作的努力使得现在所有的物价指数能温和的成长表示感谢，尽管如此，我们仍对未来的通膨趋势感到悲观，就像是童真一样，稳定的物价只能维持现状，却没有办法使其恢復原状。

尽管通膨对投资来讲实在是太重要了，但我不会再折磨你们把我们的观点在复述一遍，因为通膨本身对大家的折磨就已足够了(若谁有被虐狂可向我索取复本)，但由于通膨间断不止的使货币贬值，公司只能尽力的使你的皮夹满满，更胜于填饱你的肚子。

#### 股权附加价值

另外一项因素可能使各位对公司投资报酬率的热情再浇上一盆冷水，人们之所以要投资公司股权而非固定收益债券的理由，系在于公司经营阶层可运用这笔资金来创造比固定利息收入更高的盈余，从而人们才愿意承担万一发生损失的风险，所以额外的风险贴水是理所当然的。

但事实真是如此吗？过去数十年来，一家公司的股东权益报酬率只要超过10%，便可以被归类为一家优良企业，所以当我们把一块钱投入到这家公司，其将来能产生的经济效益将会大于一块钱，(考量到当时长期债券殖利率约为5%，而免税公债则约3%)，因为即使考量税负成本，实际到投资人手中仍能有6%-8%。

股票市场认同这种道理，在过去的一段时间，一家股东权益报酬率达到11%的公司，其市价约可以涨到净值的1.5倍，他们之所以被认定为好公司的原因在于他们赚取的盈余远多于原先保留下来的部份，这些公司所产生的附加价值相当可观。

然而这一切已成过去，但过去所得到的经验法则却很难抛弃，“当投资大众与经营阶层一脚踏进未来，他们的脑子与神经系统却还深陷于过去。”投资大众惯于利用歷史的本益比而经营阶层则习惯用传统企业评价标准，但却不去深思其前提是否早已改变，但现状的改变极其缓慢，那么持续不断地思考便变得相当必要，而一旦变化快速，则拘泥于昨日的假设将会付出极大的代价，而目前经济步调的变动速度快到令人窒息。

去年长期债券殖利率超过16%，而免税公债则约为14%，而这些收入直接落入投资人的口袋，在此同时，美国企业的股东权益报酬率却只有14%，而且尚未考量落入投资人口袋前所须支付的税负(视被投资公司的股利政策与投资人适用的所得税率而定)。

以1981年的标准而言，投资一家美国公司一块钱所产生的经济价值还低于一块钱，(当然若投资人是免税的退休基金或慈善机构，则情况可能会好一点)，假设投资人系适用于50%税率级距，而公司把所有盈余皆发放出来，则股东的投资报酬率约略等于投资7%的免税债券，而这种情况若一直持续下去，投资人等于是套牢在一堆长期7%的免税债券一样，而它真正的价值可能连其原始投资额的一半还不到。

但如果把所有盈余都保留起来，而报酬率维持不变，则盈余每年会以14%的速度增加，又假设本益比不变，则公司的股价每年也会以14%的比例增加，但增加的部份却不算是已落入股东的口袋，因为收回去的话需要付最高约20%的资本利得税，所以不管怎么说，还是比最基本的免税公债14%低。

因此除非基本报酬率降低，否则即使公司盈余每年以14%成长，对从未能收到半毛钱股利的投资人而言，等于是一无所获，这对股东与经营阶层都是不怎么愉快的经验，而更是后者希望掩饰过去的，但不论如何，事实就是事实。

多数的美国公司都把大部份的盈余分配出去，所以算是介于两个极端的例子之间，而大部份美国公司目前的税后投资报酬率却都比投资免税债券还差，当然也有少数例外，但如今总的来说，美国公司并未为投资人贡献任何附加的价值。

但要强调的是，我并不是说所有美国公司表现的比以往差，事实上，反而是比以前还要好一点，只是最低门槛比以前提高了许多，而遗憾的是，大部分的企业对此皆束手无策，只能祈求门槛能够大幅降低，极少有产业能为股东赚取高投资报酬的。

过去的通膨经验与对未来通膨的预期将会是影响未来通膨指数的最主要(但非惟一)因素，如果长期性通膨的形成原因能有效被抑制，则门槛自然会降低，美国企业的存在价值将因此大幅改善，原本被归类为不良的企业也能转为优良的企业。

而通货膨胀对于体质不佳的企业来说，更是雪上加霜，为了要维持既有的营业规模，这类低投资报酬率的公司往往必须保留住大部分的盈余，不管对于股东的权益有多大的损害也莫可奈何。

当然理智会导引人们採取不同的方法，以一个个人套牢在殖利率5%的债券来说，就不可能将好不容易拿回的5%利息，以票面价格重新投入到原有的债券，因为类似的债券在市场上可能以四折的价格就能买到，而通常的情况下，如果果真要投资，他应该会再寻找更好的投资标的，良币是不会追逐劣币的。

这种债券投资的道理也适用于股票投资之上，理论上如果说这家公司报酬率相对较高，那么把盈余留在公司继续投资下去，但若这家公司报酬率差，那么为何不把赚的盈余分配给股东，让股东自己去寻找其他报酬率较高投资机会呢??(古经文亦赞同：有个三个僕人的寓言，老天爷让其中二个会赚钱的僕人，保留他们所赚的钱并鼓励他们扩大营业，而另外一个懒惰不会赚钱的僕人，则被严厉得逞罚并叫他把钱交给前面二个僕人管理/马修第25章。

但通膨就像叫我们透过窥镜看爱丽丝梦游仙境一样，当通膨恃虐时，体质不良的企业被迫保留它所有的每一分钱，才能辛苦地维持过去拥有的生产能力，这实在是情非得已。

通膨就像是寄生在企业体内巨大的条虫，不管被它寄生的主人身体状况如何，还是拼命的从他身上吸取养份，所以不管公司的获利到底有多少(就算没有也一样)，帐上总是会有越来越多的应收帐款、存货与固定资产以维持以往的营运规模，主人的身体越差，就表示有越多比例的养份被寄生虫吸走。

以目前的情况来说，一家公司若只赚到8%或10%利润的话，根本不够拿来用于扩张、还债或发放实在的股利，通膨这条寄生虫早就把盘子清光光了，(而事实上，通常美国企业会利用许多方法将无法发放股利的窘境掩饰住，例如常常提出盈余转投资计划，强迫股东再投资，或是发行新股，拿张三的钱发放给李四，要小心这种必须要另外找到金主才能发放的股利)。

反观Berkshire通常会因积极而非被动的理由而保留盈余再投资，当然我们也不能免除前述通膨的威胁，我们歷史累计的报酬率21%扣除潜在的资本利得税后，持续地跨过那最低的门槛，但也只能算是低空掠过，只要在出一些差错，便可能使我们面临让股东创造负价值的窘境。这种情况不能保证完全不会发生，有可能是外在不可控因素，也有可能是我们自己内部的因素。

#### 盈余报告

下表显示Berkshire依照各个公司持股比例来列示帐面盈余的主要来源，而各个公司资本利得损失并不包含在内而是汇总于下表最后“已实现出售证券利得”一栏，虽然本表列示的方式与一般公认会计原则不尽相同但最后的损益数字却是一致的：其中Berkshire拥有Blue Chips Stamps60%的股权，而后者又拥有Wasco财务公司80%的股权。

![https://gitee.com/Skyball/Buffetts_letter_to_shareholders/raw/master/images/1981_Buffett%E2%80%99s_letter_to_shareholders_01.png](https://gitee.com/Skyball/Buffetts_letter_to_shareholders/raw/master/images/1981_Buffetts_letter_to_shareholders_01.PNG)

*1 包含购并企业商誉的摊销(如See's Candies;Mutual;Buffalio Evening News等)

*2 Illinois National Bank已于1980.12.31从波克夏脱离出去Blue Chip及Wesco两家公司因为本身是公开发行公司以规定编有自己的年报我建议大家仔细阅读。

就像先前我们所提到的，不具控制权的股权投资其已分配的盈余已列示于保险事业的投资收益之中，但未分配盈余佔本公司的重要性已不下于前面表列的帐面盈余。

![https://gitee.com/Skyball/Buffetts_letter_to_shareholders/raw/master/images/1981_Buffett%E2%80%99s_letter_to_shareholders_02.png](https://gitee.com/Skyball/Buffetts_letter_to_shareholders/raw/master/images/1981_Buffetts_letter_to_shareholders_02.PNG)

(a) 代表全部股权由波克夏及其子公司所持有

(b) 代表由波克夏子公司Blue Chip与Wesco所持有，依波克夏持股比例换算得来

由于我们具控制权与不具控制权的企业经营遍佈各行各业，所以恕我无法在此赘述，但无论如何，集团的重点一定是摆在产险/意外险之上，所以有必要对其产业未来发展加以说明。

#### 保险产业现况

“预测”如同媒体传播巨擘米高梅创办人Sam Goldwyn所说的是相当危险的，“尤其是关于对未来的预测”，(如果Berkshire的股东在过去的年报中读到本人对纺织业未来的分析后，可能也会深有同感)。

但若是有人要预测1982年的保险业核保结果可能会很惨，那就不会有什么好怕的了，因为情势已经由目前同业的杀价行为加上保险契约先天的性质获得了印证。

当许多汽车保险保单以六个月为期来订价并发售，而其他产物意外保险则以三年为期，所以意外保险保单流通的期间平均略低于十二个月，当然价格在保险合约的期间内是固定的，因此今年销售的合约(业内的说法，称之为保费收入)决定了明年保费收入水准的半数，至于另外一半则由明年签下的保险契约来决定，因此获利的情况自然而然会递延，也就是说若你在订价上犯了错误，那你所受的痛苦可能会持续一阵子。

注意下表所列为每年保费收入成长率以及其对当年与隔年度获利的影响，而结果正如同你在通膨高涨时所预期的一样，当保费收入以二位数成长，则当年与隔年的获利数字就会很好看，但若保费收入仅能以个位数成长，则表示核保结果就会变得很差。

下表反映一般同业所面临的情势，综合比率表示所有营运成本加上理赔损失佔保费收入的比率，百分之一百以下表示有承保利益，反之则表示有损失：

![https://gitee.com/Skyball/Buffetts_letter_to_shareholders/raw/master/images/1981_Buffett%E2%80%99s_letter_to_shareholders_03.png](https://gitee.com/Skyball/Buffetts_letter_to_shareholders/raw/master/images/1981_Buffetts_letter_to_shareholders_03.PNG)

诚如Pogo所说：“未来绝对不会和过去相同”。现在的订价习惯已註定日后悲惨的结果，尤其若因近几年无重大灾难所和得的喘息机会结束时。保险承保的情况会因大家运气好(而非运气坏)而变差，近几年来飓风大多仅停留在海上，同时摩托车骑士较少在路上跑，但他们不会永远都那么守规矩。

当然货币与社会(法院与陪审团对保险投保范围认定超越合约与判例的扩张)的双重通货膨胀是无法抵档的，财产的修补与人身的医疗等这些被视为保险公司的当然责任，所引发的成本将会无止尽的扩张。

若没遇上什么倒霉事(如大灾难或驾驶行为增加等)同业保费收入平均至少要增加十个百分点才能使1982年的承销比率不会再恶化(大部份同业估计承担损失每年以十个百分点成长，当然大家都期望自己公司成长较少)。每个保费收入成长的百分点都会加速影响到核保成绩恶化的程度，1981年按季的核保成绩低估了恶化的速度。

去年年报我们曾经提到许多保险公司因投资不当使得其公司财务变得很不健全，迫使他们放弃原有承保原则，不惜以低价承接保单以维持既有流动性。很明显的帐上持有不合理高估的债券的同业，为了现金週转而以明显不合理的低价大量卖出保单，他们害怕保单收入的减少更甚于核保所可能增加的损失。

然而不幸的是所有的同业皆因此受波及，因为你的价格不可能与竞争同业差得太远，这种压力未曾稍减，并迫使愈来愈多的同业跟进，盲目追求量的成长而非质的增加，同时又害怕失去的市场佔有率永远无法回復。

即使大家一致认同费率极不合理，我们认为没有一家保险业者，能够承受现金极度流出的情况下不接任何保单，而只要这种心态存在，则保单价格将持续面临调降压力。

对于专家一再认定保险产业的循环具规则性且长期而言核保损益接近两平，我们则抱持不同的看法，我们相信核保面临鉅额损失(虽然程度不一)将成为保险业界的常态，未来十年内最好的表现在以往仅能算得上是普通而已。

虽然面临持续恶化的未来，Berkshire的保险事业并无任何良方，但我们经营阶层确已尽力力争上游，虽然核保保单数量减少了，但核保损益相较于同业仍显优越。展望未来，Berkshire将维持低保单的现状，我们的财务实力使我们能保持最大的弹性，这在同业间并不多见。而将来总有一天，当同业保单接到怕之时，Berkshire财务实力将成为营运发展最有利的后盾。

其中我们不具控制权的主要股权投资的GEICO更是个中翘楚，它的营运绩效日起有功，比起其他同业的情况要好的许多，它堪称企业理念的最佳实践典范。

#### 股东指定捐赠计划

我们让使得所有股东皆能指定其个别捐赠单位的新计划受到广大回响，在932,206张有效股份中(即在本公司股份系由本人登记者)，有95.6%回復，而在即使不包含本人股份的情况下，也有超过90%的成绩。

此外有3%的股东主动写信来支持本计划，而股东参与的热烈与提供的意见，也是我们前所未见，这种自动自发的态度说明了本计划成功与否，也可看出Berkshire股东的天性。

很明显的，他们不但希望能拥有且能自由掌控其所欲捐赠金钱的去向，教授父权式的管理学院可能会惊讶的发现，没有一位股东表示希望由Berkshire的经营阶层来帮他们作决定或是依董监事捐赠比例行事(这是目前一般美国大企业普遍的作法)。

除了由Berkshire及其子公司经营阶层决定的捐献外，总计1,783,655美元的股东指定捐赠款共分配给675个慈善机关团体。除此之外，Berkshire以及其子公司仍将继续其以往由其经理人决定的捐赠惯例。

往后几年Berkshire将会因这项捐款计划获得些许的税负抵减，而每年10月10日以前，我们将会通知股东每股可捐赠的金额，你有三个礼拜的时间可以作决定，为免丧失资格，股份须确实由你本人名义登记。

对于去年这项计划我们惟一感到遗憾的是，有些股东虽然不是因为本身的错误，而无法参加，由于税务单位的解释令于10月初才下来，并规定股份若由代理人或经纪人名义登记者不适用。

在这样的情况下，我们试著与所有的股东立即连络(透过10月14号的那封信)，好让他们有机会可以赶上11月13号的过户截止日，这对那些非以自己名义持有股份的股东来说尤其重要，因为如非他们及时採取正名的动作，否则将因此丧失应享的权利。

由于时间紧迫，再加上联络前述股东仍须透过其代理人，使得部份股东没能参加，在此我们强烈呼吁那些股票经纪人尽速通知其客户，以免股东的权利被剥夺。

我们再三敦促的结果并无法让美国邮政加强，很多我们股东的营业员从来就没有告诉其客户这件消息(有许多股东表示他们是在看到了相关新闻才知道这项计划)，有的则是在过期后才拿到相关通知。

其中有家证券经纪商代表60位股东(约佔4%强股权)很明显地在接到邮件三个礼拜后，才将之转到客户的手上。讽刺的是，该公司并非所有部门皆如此懒散，转寄邮件的帐单在六天内就送到Berkshire公司。

我们之所以告诉大家这件事有两个理由(1)若你希望参加这项股东指定捐赠计划的话，请务必将你的股份在九月底以前改登记在自己的名下(2)就算你不想参加，最好还是至少将一股登记在自己的名下，如此才能确保你与其他股东一样在第一时间知道有关公司的重大消息。

最后包含这项股东指定捐赠计划在内的许多很好的idea，都是由Berkshire公司的副董事长兼Blue Chip的董事长Charlie Manger所构思，不管职称为何，Charlie跟我皆以执行合伙人的心态管理所有事业，而各位股东就像是我们一般的合伙人一样。

														董事长沃伦E· ·巴菲特